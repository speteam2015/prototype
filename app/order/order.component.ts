import {Component, OnInit} from 'angular2/core';
import {Order, OrderItem, OrderService} from '../order.service';
import {Profile, ProfileService} from '../profile.service';
import {RouteParams, Router} from 'angular2/router';

@Component({
    selector: 'order',
    templateUrl: 'app/order/order.component.html',
    styleUrls: ['app/order/order.component.css']
})

export class OrderComponent implements OnInit {

    public order: Order = new Order();
    public profiles: Profile[];
    public selectedItem: OrderItem;
    public enteringNew: boolean = false;

    constructor(private _router: Router,
                private _routeParams: RouteParams,
                private _profileService: ProfileService,
                private _orderService: OrderService) {}

    ngOnInit() {
        let id = this._routeParams.get('id');
        this._orderService.getOrder(parseInt(id)).then(order => this.order = order);
        this._profileService.getProfiles().then(profiles => this.profiles = profiles);
    }

    onSelect(item: OrderItem) { this.selectedItem = item; }

    toggleProfile(item: OrderItem, profile: Profile) {
        for (var id of item.profiles) this._profileService.modifyDebt(this._profileService.getProfile(id), -item.price / item.profiles.length); 
        var index = item.profiles.indexOf(profile.id);
        if (index == -1) item.profiles.push(profile.id);
        else item.profiles.splice(index, 1);
        for (var id of item.profiles) this._profileService.modifyDebt(this._profileService.getProfile(id), item.price / item.profiles.length); 
        this._orderService.saveHistory();
    }

    enterNewProfile() {
        this.enteringNew = true;
    }

    submitNewProfile(name: string) {
        this.enteringNew = false;
        var profile: Profile = {"id":0,"name":"","owes":0}
        profile.id = this._profileService.getID();
        profile.name = name;
        this._profileService.addProfile(profile);
    }

    getDate(order: Order) {
        return this._orderService.getDate(order);
    }
}

var EG_ORDER: Order = {
    "id": 1,
    "date": new Date("November 1, 2015 00:45:00"),
    "restaurant": "Chris' E. Coli Lab and Buffet",
    "items": [
        { "title": "Chicken", "description": "It's chicken bro, what u want from me.", "price": 8.0, "profiles": [] },
        { "title": "Porks", "description": "It's pork bro, what u want from me.", "price": 10.0, "profiles": [] },
        { "title": "Beefs", "description": "It's beef bro, what u want from me.", "price": 12.0, "profiles": [] }
    ]
};

var EG_PROFILES: Profile[] = [
    { "id": 1, "name": "Mark", "owes": 0 },
    { "id": 2, "name": "Danny", "owes": 0 },
    { "id": 3, "name": "Josh", "owes": 0 },
    { "id": 4, "name": "Sam", "owes": 0 },
    { "id": 5, "name": "Chris", "owes": 0 },
    { "id": 6, "name": "Andrew", "owes": 0 }
];