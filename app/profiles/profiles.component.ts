import {Component, OnInit} from 'angular2/core';
import {Profile, ProfileService} from '../profile.service';

@Component({
  selector: 'profiles',
  templateUrl: 'app/profiles/profiles.component.html',
  styleUrls: ['app/profiles/profiles.component.css'],
  providers: [ProfileService]
})

export class ProfilesComponent implements OnInit { 
	public profiles: Profile[];
  public new_profile: string;

  constructor(private _peopleService: ProfileService) { }

  getProfiles() {
    this._peopleService.getProfiles().then(profiles => this.profiles = profiles);
  }

  ngOnInit() {
    this.getProfiles();      
  }

  onSelect(profile: Profile) {
  }

  addProfile(profile_name: string, debt: number) {
    if(typeof(Storage) !== "undefined") {
      var profile: Profile = { "id": this._peopleService.getID(), "name": profile_name, "owes": 5 };
      this._peopleService.addProfile(profile);
    }
  }
}
