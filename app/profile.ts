export class Profile {
    id: number;
    name: string;
    owes: number;
}