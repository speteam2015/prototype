import {Component, OnInit} from 'angular2/core';
import {ProfileService} from '../profile.service';
import {Profile} from '../profile.service';

@Component({
    selector: 'debts',
    templateUrl: 'app/debts/debts.component.html',
    styleUrls: ['app/debts/debts.component.css']
})

export class DebtsComponent implements OnInit { 
	public profiles: Profile[];
    public selectedProfile: Profile;

    constructor(private _profileService: ProfileService) { }

    ngOnInit() {
        this._profileService.getProfiles().then(profiles => this.profiles = profiles);
    }

    onSelect(profile: Profile) {
        if (this.selectedProfile === profile) {
            this.selectedProfile = null;
        }
        else this.selectedProfile = profile;
    }

    getSelectedClass(profile: Profile) {
        return { 'selected': profile === this.selectedProfile };
    }

    reduce(profile: Profile, value: string) {
      this._profileService.modifyDebt(profile, -Number(value));
    }
}
