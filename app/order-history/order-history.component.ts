import {Component, OnInit} from 'angular2/core';
import {Router} from 'angular2/router';
import {Order, OrderItem, OrderService} from '../order.service';
import {Profile, ProfileService} from '../profile.service';

@Component({
  selector: 'order-history',
  templateUrl: 'app/order-history/order-history.component.html',
  styleUrls: ['app/order-history/order-history.component.css']
})

/**
 * This component is one of the two main views. It displays a list of previous
 * orders to be assigned to profiles.
 */
export class OrderHistoryComponent implements OnInit {

  public history: Order[] = [];
  public selectedOrder: Order;

  constructor(private _router: Router,
              private _profileService: ProfileService,
              private _orderService: OrderService) {}

  ngOnInit() {
    this._orderService.getOrders().then(history => this.history = history);
  }

	onSelect(order: Order) {
    this._router.navigate( ['Order', { id: order.id }] );
  }

  getDate(order: Order) {
    return this._orderService.getDate(order);
  }
}