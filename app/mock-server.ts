import {Order, OrderItem} from './order.service';
import {Profile} from './profile.service';

export var PEOPLE: Profile[] = [
    { "id": 0, "name": "Josh", "owes": 1 },
    { "id": 1, "name": "Sam", "owes": 2 },
    { "id": 2, "name": "Danny", "owes": 3 },
    { "id": 3, "name": "Andrew", "owes": 4 },
    { "id": 4, "name": "Chris", "owes": 5 },
    { "id": 5, "name": "Rowdy", "owes": 6 }
];

export var EG_ORDERS: Order[] = [
  {
    "id": 1,
    "date": new Date("November 1, 2015 00:45:00"),
    "restaurant": "Chris' E. Coli Lab and Buffet",
    "items": [
      { "title": "Chicken", "description": "It's chicken bro, what u want from me.", "price": 8.0, "profiles": [] },
      { "title": "Porks", "description": "It's pork bro, what u want from me.", "price": 10.0, "profiles": [] },
      { "title": "Beefs", "description": "It's beef bro, what u want from me.", "price": 12.0, "profiles": [] }
    ]
  },
  {
      "id": 2,
      "date": new Date("January 2, 2016 11:15:00"),
      "restaurant": "Pizza Hut",
      "items": [
          { "title": "Large Super Supreme", "description": "Pizza", "price": 15.0, "profiles": [] },
          { "title": "Small peperoni", "description": "Pizza", "price": 10.0, "profiles": [] },
          { "title": "Medium vegetarian", "description": "Pizza", "price": 8.0, "profiles": [] }
      ]
  }
];