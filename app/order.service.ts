import {Injectable} from 'angular2/core';
import {Profile} from './profile.service';
import {EG_ORDERS} from './mock-server';

export class Order {
    id: number;
    date: Date;
    restaurant: string;
    items: OrderItem[];
}

export class OrderItem {
    title: string;
    description: string;
    price: number;
    profiles: number[];
}

@Injectable()
export class OrderService {

    private HISTORY: Order[] = [];
    private loaded: boolean = false;

    loadOrders() {
        //this.HISTORY = EG_ORDERS;
        //this.loaded = true;
        if (!this.loaded) {
            if (localStorage.getItem("jescoms20805_Order") !== null) {
                this.HISTORY = JSON.parse(localStorage.getItem("jescoms20805_Order"));
            }
            else this.HISTORY = EG_ORDERS;
            this.loaded = true;
        }
        return Promise.resolve(this.HISTORY);
    }

    getOrders() {
        if (this.loaded === false) this.loadOrders();
        return Promise.resolve(this.HISTORY);
    }

    saveHistory() {
        localStorage.setItem("jescoms20805_Order", JSON.stringify(this.HISTORY));
    }

    getOrder(id: number) {
        if (this.loaded === false) this.loadOrders();
        for (var i = 0; i < this.HISTORY.length; i++) {
            if (this.HISTORY[i].id === id) console.log(this.HISTORY[i]);
            if (this.HISTORY[i].id === id) return Promise.resolve(this.HISTORY[i]);
        }
    }

    getDate(order: Order) {
        return moment(order.date).format("YYYY-MM-DD");
    }
}