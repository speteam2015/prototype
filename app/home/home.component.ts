import {Component} from 'angular2/core';
import {RouteConfig, RouteParams, Router, ROUTER_DIRECTIVES} from 'angular2/router';
import {OrderService} from '../order.service';
import {ProfileService} from '../profile.service';
import {DebtsComponent} from '../debts/debts.component';
import {OrderComponent} from '../order/order.component';
import {OrderHistoryComponent} from '../order-history/order-history.component';
import {ProfilesComponent} from '../profiles/profiles.component';

@Component({
    selector: 'home',
    templateUrl: 'app/home/home.component.html',
    styleUrls: ['app/home/home.component.css'], 
    directives: [ROUTER_DIRECTIVES],
    providers: [OrderService, ProfileService]
})
@RouteConfig([
	{path: '/debts',         name: 'Debts',        component: DebtsComponent }, 
	{path: '/order-history', name: 'OrderHistory', component: OrderHistoryComponent}, 
	{path: '/order/:id',    name: 'Order',        component: OrderComponent}
])

export class HomeComponent{

}
