import {Injectable} from 'angular2/core';

export class Profile {
    id: number;
    name: string;
    owes: number;
}

export var guy : Profile = {"id": 1, "name": "Bob", "owes": 5}

@Injectable()
export class ProfileService { 

  private PROFILES: Profile[] = [];
  private profile_count: number = 0;
  private loaded: boolean = false;

	getProfiles() {
    //localStorage.clear(); // Use temporarily to wipe the local store quickly

    // Load data if not yet done so
    if (!this.loaded) {  
      if (localStorage.getItem("jescoms20805_delineation") !== null) {
        this.PROFILES = JSON.parse(localStorage.getItem("jescoms20805_delineation"));
      }

      if (localStorage.getItem("jescoms20805_newpoint") !== null) {    
        this.profile_count = Number(localStorage.getItem("jescoms20805_newpoint"));
      }

      for (var i = 0; i < this.PROFILES.length; i++) {
        console.log(this.PROFILES[i]);
      }

      this.loaded = true;
    }
		return Promise.resolve(this.PROFILES);
	}

  addProfile(profile: Profile) {
    this.PROFILES.push(profile);
    localStorage.setItem("jescoms20805_delineation", JSON.stringify(this.PROFILES));
  }

  modifyDebt(profile: Profile, debt: number) {
    profile.owes += debt;

    // Remove profile if debt has been paid
    /*if (profile.owes === 0) {
      var index = this.PROFILES.indexOf(profile);
      if (index != undefined) {
        this.PROFILES.splice(index, 1);
      }
      
      // Can reset profile_count
      if (this.PROFILES.length === 0) {
        this.profile_count = 0;
        localStorage.setItem("jescoms20805_newpoint", this.profile_count.toString());
      }
    }*/

    localStorage.setItem("jescoms20805_delineation", JSON.stringify(this.PROFILES));
  }

  getID() {
    this.profile_count++;
    localStorage.setItem("jescoms20805_newpoint", this.profile_count.toString());
    return this.profile_count-1;
  }

  getProfile(id: number) {
    for (var p of this.PROFILES) if (p.id === id) return p;
    return null;     
  }
  
}